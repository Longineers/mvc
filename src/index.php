<?php
require_once 'Model.php';
require_once 'View.php';
require_once 'Controller.php';

$oModel      = new Model( 'Look At This!' );
$oController = new Controller( $oModel );
$oView       = new View( $oModel );

$sAction = isset( $_GET['action'] ) ? $_GET['action'] : null;
if ( !empty( $sAction ) ) {
  $oController->{$sAction}();
}
echo $oView->show();
