<?php
require_once '../src/View.php';

class ViewTest extends PHPUnit_Framework_TestCase {
  
  public function testMustHaveModelAndController() {
    $this->assertClassHasAttribute   ( 'oModel'     , 'View' );
    $this->assertClassNotHasAttribute( 'oController', 'View' );
  }
  
  public function testShowModel() {
    $sExpected = 'Somethingtolookoutfor';
    $oModel    = new Model( $sExpected );
    $oView     = new View( $oModel, new Controller( $oModel ) );
    $this->assertContains( $sExpected, $oView->show() ); 
  }
}