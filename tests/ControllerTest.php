<?php

require_once '../src/Controller.php';

class ControllerTest extends PHPUnit_Framework_TestCase {
  
  public function testHasModel() {
    $this->assertClassHasAttribute( 'oModel', 'Controller' );
  }
  
  public function testClickedHandler() {
    $oModel      = new Model();
    $oController = new Controller( $oModel );
    $oController->click();
      
    $sExpected   = 'Updated Data';
    $this->assertContains( $sExpected, $oModel->getString() );
  }
}

